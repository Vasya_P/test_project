FROM python:3
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /transfer_money
 WORKDIR /transfer_money
 ADD . /transfer_money/
 RUN pip install -r requirements.txt
 CMD [ "./manage.py", "migrate"]
 CMD ["./manage.py", "loaddata", "dump.json" ]