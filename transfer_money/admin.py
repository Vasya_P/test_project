from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from .models import User


class UserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


@admin.register(User)
class UserAdmin(UserAdmin):
    form = UserChangeForm

    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('inn', 'balance')}),
    )
    list_display = ('email', 'first_name', 'last_name', 'inn', 'balance', 'is_active', 'date_joined', 'is_staff')
