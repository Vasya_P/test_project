from django import forms

from .models import User


class TransferMoneyForm(forms.Form):
    users = forms.ModelChoiceField(queryset=User.objects.all(), label='Пользователи')
    inn = forms.CharField(max_length=12, label='ИНН')
    ammount = forms.DecimalField(max_digits=6, decimal_places=2, label='Сумма')
