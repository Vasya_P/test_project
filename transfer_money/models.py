from decimal import Decimal

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db import transaction

from .exceptoins import NotEnoughMoney, UsersDoesNotExist


class User(AbstractUser):
    inn = models.TextField(max_length=12, verbose_name="ИНН")
    balance = models.DecimalField(default=Decimal("0.00"), max_digits=6, decimal_places=2, verbose_name="Баланс")

    def transfer_money(self, ammount: Decimal, inn: str):
        users = User.objects.select_for_update().filter(inn=inn)
        with transaction.atomic():
            if self.balance - ammount < 0:
                raise NotEnoughMoney('Not enough money')
            if not users.exists():
                raise UsersDoesNotExist
            self.balance -= ammount
            self.save()
            arrival = ammount / users.count()
            users.update(balance=models.F('balance') + arrival)
