from django.core.exceptions import ValidationError
from django.forms import DecimalField
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from .serializers import UserSerializer
from ..exceptoins import NotEnoughMoney, UsersDoesNotExist
from ..models import User


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = []

    @action(detail=True, methods=['post', ])
    def transfer_money(self, request, pk=None):
        user = self.get_object()
        inn = request.data.get('inn')
        ammount = request.data.get('ammount')
        decimal_field = DecimalField(max_digits=6, decimal_places=2)

        if not (inn or ammount):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            ammount = decimal_field.clean(ammount)
        except ValidationError:
            return Response(data={'error': 'Неправильная сумма'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            user.transfer_money(inn=inn, ammount=ammount)
        except NotEnoughMoney:
            return Response(data={'error': 'Нехватает денег'}, status=status.HTTP_400_BAD_REQUEST)
        except UsersDoesNotExist:
            return Response(data={'error': 'Пользователи с таким ИНН не найдены'}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)
