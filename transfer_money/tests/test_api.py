from decimal import Decimal

from django.db.models import Sum
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import User


class ApiTests(APITestCase):
    fixtures = ['dump.json']

    def test_success_transfer(self):
        inn = '123456789012'
        ammount = Decimal('1000.00')
        from_user = User.objects.get(id=5)
        from_user_balance = from_user.balance
        to_users = User.objects.filter(inn=inn)
        to_users_balance = to_users.aggregate(Sum('balance'))['balance__sum']

        url = f"{reverse('user-list')}{from_user.id}/transfer_money/"
        response = self.client.post(url, format='json', data={'inn': inn, 'ammount': ammount})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        from_user.refresh_from_db()
        self.assertEqual(from_user_balance, from_user.balance + ammount)
        self.assertEqual(to_users_balance + ammount, to_users.aggregate(Sum('balance'))['balance__sum'])

    def test_fail_transfer(self):
        inn = '123456789012'
        ammount = Decimal('100000.00')
        from_user = User.objects.get(id=5)
        url = f"{reverse('user-list')}{from_user.id}/transfer_money/"
        response = self.client.post(url, format='json', data={'inn': inn, 'ammount': ammount})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
