from decimal import Decimal

from django.test import TestCase

from ..exceptoins import NotEnoughMoney, UsersDoesNotExist
from ..models import User


class UserTest(TestCase):
    fixtures = ['dump.json']

    def test_transfer_money_success(self):
        inn = '123456789012'

        ammount = Decimal('500.00')
        from_user = User.objects.get(id=5)
        try:
            from_user.transfer_money(ammount=ammount, inn=inn)
        except Exception as e:
            self.fail(e.__str__())

    def test_transfer_money_fail(self):
        from_user = User.objects.get(id=4)
        ammount = Decimal('100.00')
        fail_ammount = Decimal('100000.00')
        inn = '123456789012'
        fail_inn = '000000000000'
        self.assertRaises(NotEnoughMoney, from_user.transfer_money, fail_ammount, inn)
        from_user.balance = ammount
        from_user.save()
        self.assertRaises(UsersDoesNotExist, from_user.transfer_money, ammount, fail_inn)
        from_user.refresh_from_db()
        self.assertEqual(from_user.balance, ammount)
