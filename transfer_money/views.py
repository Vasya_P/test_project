from django.views.generic.edit import FormView

from .forms import TransferMoneyForm


class TransferMoneyView(FormView):
    template_name = 'transfer_money.html'
    form_class = TransferMoneyForm
